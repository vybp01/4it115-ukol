package main.java.logika;

public class Mesto {

public String jmeno;
public int x;
public int y;
	
	public Mesto(String jmeno, int x, int y) {
		this.jmeno = jmeno;
		this.x = x;
		this.y = y;
		
	}
	
	public int getX() {
    	return x;
    }
    
    public int getY() {
    	return y;
    }
    
    public String getJmeno() {
    	return jmeno;
    }
}
