package main.java.logika;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;;

public class Mapa extends Observable {
private Mesto hledaneMesto;
private List<Mesto> seznamMest = new ArrayList<Mesto>();
private List<Mesto> uzHledane = new ArrayList<Mesto>();


	public Mapa() {
		zalozMapu();
	}

	public void zalozMapu() {
	Mesto praha = new Mesto("Praha",215,148);
	Mesto brno = new Mesto("Brno",366,270);
	Mesto ostrava = new Mesto("Ostrava",528,179);
	Mesto plzen = new Mesto ("Plze�",134,189);
	Mesto liberec = new Mesto("Liberec",259,71);
	Mesto hradec = new Mesto("Hradec Kr�lov�",317,128);
	Mesto opava = new Mesto("Opava",498,162);
	Mesto usti = new Mesto("�st� nad Labem",163,89);
	Mesto budejovice = new Mesto("�esk� Budejovice",220,266);
	Mesto tabor = new Mesto("T�bor",228,226);
	
	seznamMest.clear();
	
	seznamMest.add(praha);
	seznamMest.add(brno);
	seznamMest.add(ostrava);
	seznamMest.add(plzen);
	seznamMest.add(liberec);
	seznamMest.add(hradec);
	seznamMest.add(opava);
	seznamMest.add(usti);
	seznamMest.add(budejovice);
	seznamMest.add(tabor);
	
	}
	
	public Mesto getHledaneMesto() {
		return hledaneMesto;
	}
	
	public void setNahodneMesto() {
		int i = 0 + (int)(Math.random()*((seznamMest.size()-1 - 0) + 1));
		hledaneMesto = seznamMest.get(i);
		seznamMest.remove(i);
		
	}
	 public boolean jeKonec() {
		 if (seznamMest.size() == 0) {
			 return true;
		 }else {
			 return false;
		 }
		 
	 }
}
