package main.java.logika;

public class Hra {
	private Mapa mapa;
	
	public Hra() {
        mapa = new Mapa();
	}

	public Mapa getMapa() {
		return mapa;
	}

	public double spoctiVzdalenost(double x, double y) {
		double xRealne = getMapa().getHledaneMesto().getX();
		double yRealne = getMapa().getHledaneMesto().getY();
		double deltaX = xRealne - x;
		double deltaY = yRealne - y;
		// zobrat suradnice z mysky a vratit do rovnice euklidovskej vzdialenosti
		// konstanta 1,087 je hruby prevod pixelu do kilometru
		double vzdalenost = Math.sqrt(deltaX*deltaX + deltaY*deltaY)/1.087;
		return vzdalenost;
	}
	
	public String vratText(double x, double y, double vzdalenost) {
	return "Realne souradnice: " + System.lineSeparator() + "X: " +
	getMapa().getHledaneMesto().getX() + " Y: " + getMapa().getHledaneMesto().getY() + 
	System.lineSeparator() + "Hadane souradnice: " + System.lineSeparator() + 
	"X: " + x + " Y: " + y + System.lineSeparator() + "Vzdalenost: " + vzdalenost + " km";
	}
	
	public double vratSkore(double vzdalenost) {
		double skore = 0;
		skore = skore + (439 - vzdalenost)/10*2.27790432802;
		return skore;
	}
	
	public String vratSkoreText(double skore) {
		return "Skore: " + skore;
	}
	
	public String vratHledaneMesto() {
		return "Hledane mesto: " + getMapa().getHledaneMesto().getJmeno();
	}
	
	public String vratKonecHry() {
		return "Konec Hry";
	}
	
}
