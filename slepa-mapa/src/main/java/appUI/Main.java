package main.java.appUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.appUI.HomeController;
import main.java.logika.Hra;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("../../resources/homectrl.fxml"));
			loader.load();
			HomeController controller = loader.getController();
			VBox root = (VBox) loader.getRoot();
			Scene scene = new Scene(root, 604, 448);
			scene.getStylesheets().add(getClass().getResource("../../resources/application.css").toExternalForm());
			
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
			Hra hra = new Hra();
			controller.init(hra);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
			launch(args);
		}
}
