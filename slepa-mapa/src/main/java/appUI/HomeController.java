package main.java.appUI;

import java.util.Observable;
import java.util.Observer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import main.java.logika.*;


public class HomeController implements Observer  {
	private Hra hra;
	@FXML
	ImageView img;
	@FXML
	TextArea vystup;
	@FXML
	TextArea scoreBoard;
	@FXML
	Button dalsiKolo;
	@FXML
	MenuItem novaHra;
	@FXML
	MenuItem konecHry;
	@FXML
	
	
	
	public void init (Hra hra) {
		this.hra = hra;
		hra.getMapa().setNahodneMesto();
		vystup.setText(hra.vratHledaneMesto());
		dalsiKolo.setDisable(true);
		dalsiKolo.setVisible(false);
		//upravit
		
		novaHra.setOnAction((event) -> {
			hra.getMapa().zalozMapu();
        	vystup.setText("Nov� hra"+ System.lineSeparator() + hra.vratHledaneMesto());    
		});
  
		konecHry.setOnAction((event) -> {
		System.exit(0);
		});
		
		img.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent event) {
				dalsiKolo.setDisable(true);
				dalsiKolo.setVisible(false);
				double x = event.getX();
				double y = event.getY();
				double vzdalenost = hra.spoctiVzdalenost(x, y);
				vystup.setText(hra.vratText(x,y, vzdalenost));			
				scoreBoard.setText(hra.vratSkoreText(hra.vratSkore(vzdalenost)));
				dalsiKolo.setDisable(false);
				dalsiKolo.setVisible(true);
				event.consume();
			}
			});	
		dalsiKolo.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
            	if(hra.getMapa().jeKonec()) {
            		vystup.setText(hra.vratKonecHry());
            		dalsiKolo.setDisable(true);
            		dalsiKolo.setVisible(false);
            	}else {
            		hra.getMapa().setNahodneMesto();
            		dalsiKolo.setDisable(true);
            		dalsiKolo.setVisible(false);
            		vystup.setText(hra.vratHledaneMesto());
            	}
            }
       });
	}
	
	
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		//vloz veci co se maji updatenout kdyz dojde k zmene akce
	}
}
